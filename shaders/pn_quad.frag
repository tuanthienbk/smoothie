#version 400 core

// Ouput data
out vec3 color;
in vec3 teNormal;		 // interpolated from the tesselation evaluation shader
in vec3 tePos;			// interpolated from the tesselation evaluation shader
in vec2 teTexture;
uniform sampler2D myTextureSampler;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
uniform vec3 LightPosition_worldspace;

void main()
{
//	color = PhongShading(teNormal,tePos,vec2(0,0));
	// Light emission properties
	// You probably want to put them as uniforms
	vec3 LightColor = vec3(1.,1.,1.);
	float LightPower = 50.0f;

	// Material properties
	vec3 MaterialDiffuseColor = texture2D( myTextureSampler, teTexture ).rgb;
//	vec3 MaterialDiffuseColor = vec3(1.,.5,.5);
	vec3 MaterialAmbientColor = vec3(0.5,0.5,0.) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	// Distance to the light
	float distance = length( LightPosition_worldspace - tePos );

	// Normal of the computed fragment, in camera space
	vec3 n = teNormal;
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	// Cosine of the angle between the normal and the light direction,
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( n,l ), 0,1 );

	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( dot( E,R ), 0,1 );

	color =
		// Ambient : simulates indirect lighting
		MaterialAmbientColor +
		// Diffuse : "color" of the object
		MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance) +
		// Specular : reflective highlight, like a mirror
		MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,5) / (distance*distance);
}
