#version 440 core

layout (location = 0) in vec3 vertexPosition;

uniform mat4 Projection;
uniform mat4 ModelView;

void main()
{
    gl_Position = Projection*ModelView*vec4(vertexPosition,1);
    gl_PointSize = 10.0;
}
