#version 440 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertex_modelspace;
layout(location = 1) in vec3 normal_modelspace;
layout(location = 2) in vec2 vertexUV;
// Values that stay constant for the whole mesh.

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexture;

//uniform mat4 ProjectionMatrix;
//uniform mat4 ModelViewMatrix;
//uniform mat4 NormalMatrix;

//out vec3 EyeDirection_cameraspace;
//out vec3 LightDirection_cameraspace;
//uniform vec3 LightPosition_worldspace;

void main(){
  
  vPosition = vertex_modelspace;
  vNormal = normal_modelspace;
//  gl_Position = ProjectionMatrix*ModelViewMatrix*vec4(vertex_modelspace,1);
//  vPosition = (ProjectionMatrix*ModelViewMatrix*vec4(vertex_modelspace,1)).xyz;
//  vNormal = normalize((NormalMatrix*vec4(normal_modelspace,0)).xyz);
  vTexture = vertexUV;
//  EyeDirection_cameraspace = vec3(0,0,0) - vPosition;
//  vec3 LightPosition_cameraspace = ( ModelViewMatrix * vec4(LightPosition_worldspace,1)).xyz;
//  LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
//  gl_PointSize = 1.0;
}

