#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec4 tePatchDistance[3];
//in vec3 tePosition[3];
in vec3 teNormal[3];
out vec3 gFacetNormal;
out vec4 gPatchDistance;
out vec3 gTriDistance;

//uniform mat4 NormalMatrix;
void main()
{
//	vec3 A = tePosition[2] - tePosition[0];
//	vec3 B = tePosition[1] - tePosition[0];
//	gFacetNormal = (NormalMatrix*vec4(normalize(cross(A, B)),0)).xyz;

    gFacetNormal = teNormal[0];
    gPatchDistance = tePatchDistance[0];
    gTriDistance = vec3(1, 0, 0);
//    gPosition = gl_in[0].gl_Position.xyz;
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gFacetNormal = teNormal[1];
    gPatchDistance = tePatchDistance[1];
    gTriDistance = vec3(0, 1, 0);
//    gPosition = gl_in[1].gl_Position.xyz;
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gFacetNormal = teNormal[2];
    gPatchDistance = tePatchDistance[2];
    gTriDistance = vec3(0, 0, 1);
//    gPosition = gl_in[2].gl_Position.xyz;
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}


