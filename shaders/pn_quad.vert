#version 400 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertex_modelspace;
layout(location = 1) in vec3 normal_modelspace;
layout(location = 2) in vec2 vertexUV;
// Values that stay constant for the whole mesh.

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexture;

void main(){

  vPosition = vertex_modelspace;
  vNormal = normal_modelspace;
  vTexture = vertexUV;

//  gl_PointSize = 1.0;
}

