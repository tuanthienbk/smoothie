#version 400 core

// Ouput data
out vec4 FragColor;
in vec3 gFacetNormal;
in vec4 gPatchDistance;
in vec3 gTriDistance;

in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

uniform int DrawLines;

float amplify(float d, float scale, float offset)
{
    d = scale * d + offset;
    d = clamp(d, 0, 1);
    d = 1 - exp2(-2*d*d);
    return d;
}

void main()
{
	 vec3 N = normalize(gFacetNormal);
	 vec3 L = vec3(.1,.1,1.);
	 vec3 E = vec3(0,0,1);
	 vec3 H = normalize(L + E);
	 float df = abs(dot(N, L));
	 float sf = abs(dot(N, H));
	 sf = pow(sf, 5);
	 vec3 color = vec3(0.1, 0.1, 0.1) + df * vec3(.5, .5, .5) +  sf * vec3(.3, .3, .3);
	 if (DrawLines > 0) {
		float d1 = min(min(gTriDistance.x, gTriDistance.y), gTriDistance.z);
		float d2 = min(min(min(gPatchDistance.x, gPatchDistance.y), gPatchDistance.z), gPatchDistance.w);
		d1 = 1 - amplify(d1, 50, -0.5);
		d2 = amplify(d2, 50, -0.5);
		color = d2 * color + d1 * d2 * vec3(1, 1, 1);
	 }
	 FragColor = vec4(color, 1.0);

}
