#version 400 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 ctrlPnts;
layout(location = 1) in int valence;
//layout(location = 1) in vec2 vertexUV;
// Values that stay constant for the whole mesh.

//layout(location = 0) in vec3 vertex_modelspace;
//layout(location = 1) in vec3 normal_modelspace;
//layout(location = 2) in vec2 vertexUV;

out vec3 vPosition;
out int vVal;
//out vec2 vTexture;

//uniform mat4 ModelViewProjectionMatrix;
//uniform mat4 ModelViewMatrix;
//uniform mat4 ModelMatrix;
//uniform mat4 NormalMatrix;

void main(){

  vPosition = ctrlPnts;
  vVal = valence;

//  gl_Position = ModelViewProjectionMatrix*vec4(vPosition,1);
//  gl_PointSize = 5.0;
}

