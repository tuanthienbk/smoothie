#ifndef VISUALOBJECT_H
#define VISUALOBJECT_H

#include <iostream>
#include <fstream>
#include <stdio.h>

#include "common.h"

class VisualObject
{
private:
    typedef struct {
        GLenum       type;
        const char*  filename;
        GLuint       shader;
    } ShaderInfo;

    GLuint LoadShaders( ShaderInfo* shaders )
    {
        if ( shaders == NULL ) { return 0; }

        GLuint program = glCreateProgram();

        ShaderInfo* entry = shaders;
        while ( entry->type != GL_NONE ) {
            GLuint shader = glCreateShader( entry->type );

            entry->shader = shader;

            std::string ShaderCode;
            std::ifstream ShaderStream(entry->filename, std::ios::in);
            if(ShaderStream.is_open()){
                std::string Line = "";
                while(getline(ShaderStream, Line))
                    ShaderCode += "\n" + Line;
                ShaderStream.close();
            }else{
                printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", entry->filename);
                getchar();
                return -1;
            }

            // Compile Shader
            printf("Compiling shader : %s\n", entry->filename);
            char const * SourcePointer = ShaderCode.c_str();
            glShaderSource(shader, 1, &SourcePointer , NULL);
            glCompileShader(shader);

            // Check Shader
            GLint Result = GL_FALSE;
            int InfoLogLength;
            glGetShaderiv(shader, GL_COMPILE_STATUS, &Result);
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &InfoLogLength);
            if ( InfoLogLength > 0 ){
                std::vector<char> shaderErrorMessage(InfoLogLength+1);
                glGetShaderInfoLog(shader, InfoLogLength, NULL, &shaderErrorMessage[0]);
                printf("%s\n", &shaderErrorMessage[0]);
            }

            glAttachShader( program, shader );
            ++entry;
        }

        glLinkProgram( program );
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
    #ifdef _DEBUG
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cerr << "Shader linking failed: " << log << std::endl;
            delete [] log;
    #endif /* DEBUG */

            for ( entry = shaders; entry->type != GL_NONE; ++entry ) {
                glDeleteShader( entry->shader );
                entry->shader = 0;
            }

            return 0;
        }

        return program;
    }
public:
    VisualObject(){
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
    }
    void setShaderFromFile(const char* vertexShader, const char* fragmentShader)
    {
        ShaderInfo shader_info[] =
        {
            { GL_VERTEX_SHADER, vertexShader },
            { GL_FRAGMENT_SHADER, fragmentShader },
            { GL_NONE, NULL }
        };
        program = LoadShaders(shader_info);
    }
    void setShaderFromFile(const char* vertexShader, const char* fragmentShader, const char* tessControlShader, const char* tessEvalShader)
    {
        ShaderInfo shader_info[] =
        {
            { GL_VERTEX_SHADER, vertexShader },
            { GL_TESS_CONTROL_SHADER, tessControlShader },
            { GL_TESS_EVALUATION_SHADER, tessEvalShader },
            { GL_FRAGMENT_SHADER, fragmentShader },
            { GL_NONE, NULL }
        };
        program = LoadShaders(shader_info);
    }
    void setShaderFromFile(const char* vertexShader, const char* fragmentShader, const char* tessControlShader, const char* tessEvalShader, const char* geometryShader)
    {
        ShaderInfo shader_info[] =
        {
            { GL_VERTEX_SHADER, vertexShader },
            { GL_TESS_CONTROL_SHADER, tessControlShader },
            { GL_TESS_EVALUATION_SHADER, tessEvalShader },
            { GL_GEOMETRY_SHADER, geometryShader },
            { GL_FRAGMENT_SHADER, fragmentShader },
            { GL_NONE, NULL }
        };
        program = LoadShaders(shader_info);
    }
    virtual ~VisualObject()
    {
        glDeleteProgram(program);
        glDeleteVertexArrays(1, &vao);
    }
    virtual void beginRender()
    {
        glUseProgram(program);
    }
    virtual void endRender()=0;
    
    void setMatrix(GLfloat* matrix, const char* name) {
        GLuint MatrixID = glGetUniformLocation(program, name);
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, matrix);
    }
    void setVector(GLfloat x, GLfloat y, GLfloat z, const char* name) {
        GLuint VectorID = glGetUniformLocation(program, name);
        glUniform3f(VectorID, x,y,z);
    }

protected:
    GLuint vao,program;

};

#endif // VISUALOBJECT_H
