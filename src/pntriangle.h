#ifndef PNTRIANGLE_H
#define PNTRIANGLE_H

#include "visualobject.h"
#include "trianglemesh.h"

class PNTriangle: public VisualObject
{
public:
    PNTriangle(TriangleMesh* tm):
        VisualObject(),
        mesh(tm),
        hasTexture(false)
    {
        init();
    }

    PNTriangle(TriangleMesh* tm, const char* tex_file):
        VisualObject(),
        mesh(tm)
    {
        init();
        setTexture(tex_file);
    }

    PNTriangle() = delete;

    void setTexture(const char* tex_file) {
        if (mesh->tex_coords.size() == mesh->nV) {
            texture = smooth::mesh::loadDDS(tex_file);
            glGenBuffers(1, &t_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, t_vbo);
            glBufferData(GL_ARRAY_BUFFER, mesh->nV*sizeof(Vec2r), mesh->tex_coords.data(), GL_STATIC_DRAW);
            hasTexture = true;
        } else {
            cerr << "texture coordinate is not found, generate it first" << endl;
        }
    }

    virtual ~PNTriangle()
    {
        glDeleteBuffers(1, &m_vbo);
        glDeleteBuffers(1, &n_vbo);
        glDeleteBuffers(1, &i_vbo);
        glDeleteProgram(program);
        if (hasTexture)
        {
            glDeleteBuffers(1, &t_vbo);
            glDeleteTextures(1, &texture);
        }
    }
    virtual void endRender()
    {
        glBindVertexArray( vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, n_vbo);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_vbo);

        if (hasTexture)
        {
            // Bind our texture in Texture Unit 0
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture);
            GLuint TextureID  = glGetUniformLocation(program, "myTextureSampler");
            glUniform1i(TextureID, 0);

            glEnableVertexAttribArray(2);
            glBindBuffer(GL_ARRAY_BUFFER, t_vbo);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }

        glPatchParameteri(GL_PATCH_VERTICES, 3);
        glDrawElements(GL_PATCHES, 3*mesh->nF, GL_UNSIGNED_INT, 0);
//        glDrawElements(GL_TRIANGLES, 3*mesh->nF, GL_UNSIGNED_INT, 0);


        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);

    }

protected:
    GLuint m_vbo, i_vbo, n_vbo, t_vbo, texture;
    bool hasTexture = false;
    TriangleMesh *mesh;

private:
    void init()
    {
        setShaderFromFile(PROJECT_PATH"/shaders/pn_triangles.vert",
                          PROJECT_PATH"/shaders/pn_triangles.frag",
                          PROJECT_PATH"/shaders/pn_triangles.tess",
                          PROJECT_PATH"/shaders/pn_triangles.eval");

        glGenBuffers(1, &m_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo );
        glBufferData(GL_ARRAY_BUFFER, mesh->nV*sizeof(Vec3r), mesh->vertices.data(), GL_STATIC_DRAW);

        glGenBuffers(1, &i_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_vbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->nF*sizeof(Vec3i), mesh->faces.data(), GL_STATIC_DRAW);

//        for(int i = 0; i < mesh->nF; i++) {
//            cout << mesh->faces[i][0] <<" " << mesh->faces[i][1] <<" " << mesh->faces[i][2] << endl;
//        }
        glGenBuffers(1, &n_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, n_vbo);
        glBufferData(GL_ARRAY_BUFFER, mesh->nV*sizeof(Vec3r), mesh->normals.data(), GL_STATIC_DRAW);

//        for(int i = 0; i < mesh->nV; i++) {
//            cout << mesh->vertices[i][0] <<" "  << mesh->vertices[i][1] <<" " << mesh->vertices[i][2];
//            cout <<" ; "<< mesh->tex_coords[i][0] <<" "  << mesh->tex_coords[i][1];
//            cout <<" ; "<< mesh->normals[i][0] <<" "  << mesh->normals[i][1] <<" " << mesh->normals[i][2] << endl;
//        }

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }


};
#endif // PNTRIANGLE_H
