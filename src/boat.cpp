// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "pntriangle.h"
#include "pnquad.h"
#include "plot3d.h"

int initWindow()
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( 1024, 768, "Boat", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    printf("%s %s %s \n", glGetString(GL_VERSION), glGetString(GL_VENDOR), glGetString(GL_RENDERER));

    // Ensure we can capture the escape key being pressed below

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    glfwSetCursorPos(window, 1024/2, 768/2);

    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(10.0);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);

}

int main()
{
    initWindow();

    TriangleMesh *tmesh = new TriangleMesh(PROJECT_PATH"/models/boat_tri.obj");
    PNTriangle *tvisual = new PNTriangle(tmesh);
    tvisual->setTexture(PROJECT_PATH"/models/wood3.dds");

//    QuadMesh *qmesh = new QuadMesh(PROJECT_PATH"/models/boat_quad.obj");
//    PNQuad *qvisual = new PNQuad(qmesh);
//    qvisual->setTexture(PROJECT_PATH"/models/wood3.dds");

    glm::mat4 P = glm::perspective(45.0f, 3.0f / 3.0f, 0.1f, 100.0f);
    glm::mat4 V       = glm::lookAt(
                glm::vec3(0.,0.,5.), // Camera position in World Space
                glm::vec3(0.,0.,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );
    glm::mat4 M      = glm::mat4(1.0f);
    glm::mat4 MV        =  V * M;
    do{
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        MV = glm::rotate(MV, 1.0f, glm::vec3(1.0f,0,0));
        glm::mat4 NM = glm::transpose(glm::inverse(MV));
//        ctrl_polygon->beginRender();
//        ctrl_polygon->setMatrix(&MV[0][0],"ModelView");
//        ctrl_polygon->setMatrix(&P[0][0],"Projection");
//        ctrl_polygon->endRender();

        tvisual->beginRender();
        tvisual->setMatrix(&MV[0][0],"ModelViewMatrix");
        tvisual->setMatrix(&P[0][0],"ProjectionMatrix");
        tvisual->setMatrix(&NM[0][0],"NormalMatrix");
        tvisual->setVector(0.,0.,5.,"LightPosition_worldspace");
        tvisual->endRender();
        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 );

    delete tvisual;
    delete tmesh;
//    delete qvisual;
//    delete qmesh;
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    return 0;
}

