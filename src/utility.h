#ifndef ULTILITY_H
#define ULTILITY_H

#include "common.h"
#include <Eigen/SparseCore>

namespace smooth {
namespace mesh {

void buildHalfEdge(Eigen::SparseMatrix<int> &edges, int nV, int nF,
                   vector<int>& twin, vector<int>& edge_map, int ngon);

bool loadOBJ( const char * path,
              VecCoord3& out_vertices,
              VecCoord2& out_uvs,
              VecCoord3& out_normals,
              VecCoord3i& out_faces,
              vector<int>& out_twin,
              vector<int>& out_edgemap
              );

bool loadOBJQuad(const char * path,
                 VecCoord3& out_vertices,
                 VecCoord2& out_uvs,
                 VecCoord3& out_normals,
                 VecCoord4i& out_faces,
                 vector<int>& out_twin,
                 vector<int>& out_edgemap
                 );

bool loadOFF(const char* path,
             VecCoord3 &out_vertices,
             VecCoord3i &out_faces,
             vector<int>& out_twin,
             vector<int>& out_edgemap);

bool loadOFFQuad(const char* path,
             VecCoord3 &out_vertices,
             VecCoord4i &out_faces,
             vector<int>& out_twin,
             vector<int>& out_edgemap);


unsigned int loadDDS(const char * path);
}
}

#endif
