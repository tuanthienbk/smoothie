#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "utility.h"

namespace smooth {
namespace mesh {

void buildHalfEdge(Eigen::SparseMatrix<int> &edges, int nV, int nF,
                   vector<int>& twin, vector<int>& edge_map, int ngon)
{
    twin.resize(ngon*nF);
    edge_map.resize(nV);
    for (int i=0; i < nV; ++i)
    {
        Eigen::SparseMatrix<int>::InnerIterator it(edges,i);
        edge_map[i] = it.value() - 1;
        for(;it;++it)
        {
            int ie = edges.coeff(it.col(),it.row())-1;
            twin[it.value()-1] = ie;
            if (ie < 0) edge_map[i] = it.value() - 1;
        }
    }
}

// simple OBJ loader.
bool loadOBJ(const char * path,
             VecCoord3& out_vertices,
             VecCoord2& out_uvs,
             VecCoord3& out_normals,
             VecCoord3i& out_faces,
             vector<int>& out_twin,
             vector<int>& out_edgemap
             )
{
#if NDEBUG
    cout << "Loading OBJ file " << path <<"..." << endl;
#endif
    unordered_map<unsigned int, pair<unsigned int, unsigned int>> vertexMap;
    VecCoord2 temp_uvs;
    VecCoord3 temp_normals;
    out_vertices.clear();

    fstream file(path, ios::in);
    if( !file ){
        printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
        return false;
    }

    vector<Eigen::Triplet<int>> tripletList;
    tripletList.reserve(30000); // about 10000 faces
    int idx = 0;

    while( 1 ){
        string lineHeader;
        // read the first word of the line
        file >> lineHeader;
        if (file.eof())
            break; // EOF = End Of File. Quit the loop.

        if ( lineHeader == "v" ){
            Vec3r vertex;
            file >> vertex[0] >> vertex[1] >> vertex[2];
            out_vertices.push_back(vertex);
        }else if ( lineHeader == "vt"){
            Vec2r uv;
            file >> uv[0] >> uv[1] ;
            uv[1] = -uv[1]; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
            temp_uvs.push_back(uv);
        }else if ( lineHeader == "vn" ){
            Vec3r normal;
            file >> normal[0] >> normal[1] >> normal[2];
            temp_normals.push_back(normal);
        }else if ( lineHeader ==  "f"){
            Vec3i vertexIndex, uvIndex, normalIndex;
            char c;
            if (! (file >> vertexIndex[0] >> c >> uvIndex[0] >> c >> normalIndex[0]
                     >> vertexIndex[1] >> c >> uvIndex[1] >> c >> normalIndex[1]
                     >> vertexIndex[2] >> c >> uvIndex[2] >> c >> normalIndex[2]) )
            {
                cerr << "texture index or normal index is missing " << endl;
                return false;
            }
            vertexIndex -= Vec3i(1,1,1);
            uvIndex -= Vec3i(1,1,1);
            normalIndex -= Vec3i(1,1,1);
            out_faces.push_back(vertexIndex);

            vertexMap[vertexIndex[0]] = make_pair(uvIndex[0], normalIndex[0]);
            vertexMap[vertexIndex[1]] = make_pair(uvIndex[1], normalIndex[1]);
            vertexMap[vertexIndex[2]] = make_pair(uvIndex[2], normalIndex[2]);

            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[0],vertexIndex[1],3*idx+1));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[1],vertexIndex[2],3*idx+2));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[2],vertexIndex[0],3*idx+3));
            idx++;
        }else{
            // Probably a comment, eat up the rest of the line
            char dummy[1000];
            file.getline(dummy, 1000);
        }

    }
    file.close();
    int nV = out_vertices.size();
    int nF = out_faces.size();
    /// rearrange uvs and normals to  use only 1 indices array buffer
    out_uvs.clear();
    out_uvs.resize(nV);
    out_normals.clear();
    out_normals.resize(nV);
    // For each vertex
    for( unsigned int i=0; i<nV; i++ ){
        pair<unsigned int, unsigned int> p = vertexMap[i];
        out_uvs[i] = temp_uvs[p.first];
        out_normals[i] = temp_normals[p.second];
    }

    Eigen::SparseMatrix<int> edges(nV,nV);
    edges.setFromTriplets(tripletList.begin(), tripletList.end());
    buildHalfEdge(edges, nV, nF, out_twin, out_edgemap, 3);

    return true;
}

bool loadOBJQuad(const char * path,
                 VecCoord3& out_vertices,
                 VecCoord2& out_uvs,
                 VecCoord3& out_normals,
                 VecCoord4i& out_faces,
                 vector<int>& out_twin,
                 vector<int>& out_edgemap
                 ){
#if NDEBUG
    cout << "Loading OBJ file " << path <<"..." << endl;
#endif
    unordered_map<unsigned int, pair<unsigned int, unsigned int>> vertexMap;
    VecCoord2 temp_uvs;
    VecCoord3 temp_normals;
    out_vertices.clear();

    fstream file(path, ios::in);
    if(!file){
        cerr << "Impossible to open the file ! Are you in the right path ?" << endl;
        return false;
    }

    vector<Eigen::Triplet<int>> tripletList;
    tripletList.reserve(40000); // about 10000 faces
    int idx = 0;
    while( 1 ){
        string lineHeader;
        // read the first word of the line
        file >> lineHeader;
        if (file.eof())
            break; // EOF = End Of File. Quit the loop.

        if ( lineHeader == "v"){
            Vec3r vertex;
            file >> vertex[0] >> vertex[1] >> vertex[2];
            out_vertices.push_back(vertex);
        }else if ( lineHeader == "vt" ){
            Vec2r uv;
            file >> uv[0] >> uv[1];
            uv[1] = -uv[1]; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
            temp_uvs.push_back(uv);
        }else if ( lineHeader == "vn"){
            Vec3r normal;
            file >> normal[0] >> normal[1] >> normal[2];
            temp_normals.push_back(normal);
        }else if ( lineHeader == "f" ){
            Vec4i vertexIndex, uvIndex, normalIndex;
            char c;
            if (! ( file >> vertexIndex[0] >> c >> uvIndex[0] >> c >> normalIndex[0]
                     >> vertexIndex[1] >> c >> uvIndex[1] >> c >> normalIndex[1]
                     >> vertexIndex[2] >> c >> uvIndex[2] >> c >> normalIndex[2]
                     >> vertexIndex[3] >> c >> uvIndex[3] >> c >> normalIndex[3] ) )
             {
                cerr << "texture index or normal index is missing " << endl;
                return false;
            }
            vertexIndex -= Vec4i(1,1,1,1);
            uvIndex -= Vec4i(1,1,1,1);
            normalIndex -= Vec4i(1,1,1,1);
            out_faces.push_back(vertexIndex);

            vertexMap[vertexIndex[0]] = make_pair(uvIndex[0], normalIndex[0]);
            vertexMap[vertexIndex[1]] = make_pair(uvIndex[1], normalIndex[1]);
            vertexMap[vertexIndex[2]] = make_pair(uvIndex[2], normalIndex[2]);
            vertexMap[vertexIndex[3]] = make_pair(uvIndex[3], normalIndex[3]);

            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[0],vertexIndex[1],4*idx+1));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[1],vertexIndex[2],4*idx+2));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[2],vertexIndex[3],4*idx+3));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[3],vertexIndex[0],4*idx+4));

            idx++;

        }else{
            // Probably a comment, eat up the rest of the line
            char dummy[1000];
            file.getline(dummy, 1000);
        }

    }
    file.close();
    int nV = out_vertices.size();
    int nF = out_faces.size();
    /// rearrange uvs and normals to  use only 1 index array buffer
    out_uvs.clear();
    out_uvs.resize(nV);
    out_normals.clear();
    out_normals.resize(nV);
    // For each vertex
    for( unsigned int i=0; i<nV; i++ ){
        pair<unsigned int, unsigned int> p = vertexMap[i];
        out_uvs[i] = temp_uvs[p.first];
        out_normals[i] = temp_normals[p.second];
    }

    Eigen::SparseMatrix<int> edges(nV,nV);
    edges.setFromTriplets(tripletList.begin(), tripletList.end());
    buildHalfEdge(edges, nV, nF, out_twin, out_edgemap, 4);

    return true;
}

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

GLuint loadDDS(const char * imagepath){

    unsigned char header[124];

    FILE *fp;

    /* try to open the file */
    fp = fopen(imagepath, "rb");
    if (fp == NULL){
        printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar();
        return 0;
    }

    /* verify the type of file */
    char filecode[4];
    fread(filecode, 1, 4, fp);
    if (strncmp(filecode, "DDS ", 4) != 0) {
        fclose(fp);
        return 0;
    }

    /* get the surface desc */
    fread(&header, 124, 1, fp);

    unsigned int height      = *(unsigned int*)&(header[8 ]);
    unsigned int width	     = *(unsigned int*)&(header[12]);
    unsigned int linearSize	 = *(unsigned int*)&(header[16]);
    unsigned int mipMapCount = *(unsigned int*)&(header[24]);
    unsigned int fourCC      = *(unsigned int*)&(header[80]);


    unsigned char * buffer;
    unsigned int bufsize;
    /* how big is it going to be including all mipmaps? */
    bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
    buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
    fread(buffer, 1, bufsize, fp);
    /* close the file pointer */
    fclose(fp);

    unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4;
    unsigned int format;
    switch(fourCC)
    {
    case FOURCC_DXT1:
        format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
        break;
    case FOURCC_DXT3:
        format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
        break;
    case FOURCC_DXT5:
        format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
        break;
    default:
        free(buffer);
        return 0;
    }

    // Create one OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
    unsigned int offset = 0;

    /* load the mipmaps */
    for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
    {
        unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;
        glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
                               0, size, buffer + offset);

        offset += size;
        width  /= 2;
        height /= 2;

        // Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
        if(width < 1) width = 1;
        if(height < 1) height = 1;

    }

    free(buffer);

    return textureID;
}

bool loadOFF(const char* path,
             VecCoord3 &out_vertices,
             VecCoord3i &out_faces,
             vector<int>& out_twin,
             vector<int>& out_edgemap)
{
    FILE *offfile = fopen(path,"r");
    if (offfile == NULL)
    {
        printf("File not found! \n");
        return false;
    }
    char header[5];
    fscanf(offfile,"%s\n", header);
    if (strcmp(header,"OFF"))
    {
        printf("This is not OFF file\n");
        return false;
    }
    int nV, nF;
    fscanf(offfile,"%d %d%*[^\n]", &nV, &nF);
    out_vertices.resize(nV);
    out_faces.resize(nF);

    Eigen::SparseMatrix<int> edges(nV,nV);
    float x, y, z;
    for(int i = 0; i < nV; i++)
    {
        fscanf(offfile,"%f %f %f%*[^\n]",&x, &y, &z);
        out_vertices[i] << double(x), double(y), double(z);

    }

    vector<Eigen::Triplet<int>> tripletList;
    tripletList.reserve(3*nF);
    for(int i = 0; i < nF; i++)
    {
        int np;
        fscanf(offfile,"%d %d %d %d%*[^\n]",&np, &out_faces[i][0], &out_faces[i][1], &out_faces[i][2]);
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][0],out_faces[i][1],3*i+1));
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][1],out_faces[i][2],3*i+2));
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][2],out_faces[i][0],3*i+3));
    }
    fclose(offfile);

    edges.setFromTriplets(tripletList.begin(), tripletList.end());
    buildHalfEdge(edges, nV, nF, out_twin, out_edgemap, 3 );

    return true;
}

bool loadOFFQuad(const char* path,
             VecCoord3 &out_vertices,
             VecCoord4i &out_faces,
             vector<int>& out_twin,
             vector<int>& out_edgemap)
{
    FILE *offfile = fopen(path,"r");
    if (offfile == NULL)
    {
        printf("File not found! \n");
        return false;
    }
    char header[5];
    fscanf(offfile,"%s\n", header);
    if (strcmp(header,"OFF"))
    {
        printf("This is not OFF file\n");
        return false;
    }
    int nV, nF;
    fscanf(offfile,"%d %d%*[^\n]", &nV, &nF);
    out_vertices.resize(nV);
    out_faces.resize(nF);

    Eigen::SparseMatrix<int> edges(nV,nV);
    float x, y, z;
    for(int i = 0; i < nV; i++)
    {
        fscanf(offfile,"%f %f %f%*[^\n]",&x, &y, &z);
        out_vertices[i] << double(x), double(y), double(z);

    }

    vector<Eigen::Triplet<int>> tripletList;
    tripletList.reserve(3*nF);
    for(int i = 0; i < nF; i++)
    {
        int np;
        fscanf(offfile,"%d %d %d %d %d%*[^\n]",&np, &out_faces[i][0], &out_faces[i][1], &out_faces[i][2], &out_faces[i][3]);
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][0],out_faces[i][1],3*i+1));
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][1],out_faces[i][2],3*i+2));
        tripletList.push_back(Eigen::Triplet<int>(out_faces[i][2],out_faces[i][0],3*i+3));
    }
    fclose(offfile);

    edges.setFromTriplets(tripletList.begin(), tripletList.end());
    buildHalfEdge(edges, nV, nF, out_twin, out_edgemap, 3 );

    return true;
}
} // namespace mesh
} // namespace smooth
