#ifndef TRIANGLEMESH_H
#define TRIANGLEMESH_H

#include "utility.h"
#include <Eigen/SparseCore>

class TriangleMesh
{

public:
    /// number of vertices, faces and edges
    unsigned int nV, nF, nE;
    VecCoord3 vertices; // vertex coordinates
    VecCoord2 tex_coords; // texture coordinates
    VecCoord3 normals; // normal coordinates
    VecCoord3i faces; // face list
    vector<float> tmp;
private:
    vector<int> twin;
    vector<int> edge_map;
    vector<int> valence;
    vector<int> eops;
    int maxValence;
private:
    void computeNormals()
    {
        normals.resize(nV,Vec3r(0.0f, 0.0f, 0.0f));

        for(int i = 0; i < nF; i++) {
            Vec3r fnormal = getFaceAreaVec(i);
            normals[faces[i][0]] += fnormal;
            normals[faces[i][1]] += fnormal;
            normals[faces[i][2]] += fnormal;
        }
        for(int i = 0; i < nV; i++)
            normals[i].normalize();
    }
public:
    double getFaceArea(int fi)
    {
        return  0.5*getFaceAreaVec(fi).norm();

    }
    Vec3r getFaceAreaVec(int fi)
    {
        return (vertices[faces[fi][1]] - vertices[faces[fi][0]]).cross(vertices[faces[fi][2]] - vertices[faces[fi][0]]);
    }
public:
    //! create mesh structure from raw data
    TriangleMesh (double* v, int nv, int* f, int nf)
    {
        nV = nv;
        nF = nf;
        vertices.resize(nV);
        faces.resize(nF);

        Eigen::SparseMatrix<int> edges(nV,nV);  // edge graph

        for(int i = 0; i < nv; i++)
            vertices[i]<<v[3*i], v[3*i+1], v[3*i+2];

        vector<Eigen::Triplet<int>> tripletList;
        tripletList.reserve(3*nF);
        for(int i = 0; i < nf; i++)
        {
            faces[i] << f[3*i], f[3*i+1], f[3*i+2];
            tripletList.push_back(Eigen::Triplet<int>(faces[i][0],faces[i][1],3*i+1));
            tripletList.push_back(Eigen::Triplet<int>(faces[i][1],faces[i][2],3*i+2));
            tripletList.push_back(Eigen::Triplet<int>(faces[i][2],faces[i][0],3*i+3));
        }
        edges.setFromTriplets(tripletList.begin(), tripletList.end());
        smooth::mesh::buildHalfEdge(edges, nV, nF, twin, edge_map, 3);
        computeNormals();
        computeValence();
    }

    //! disable default constructor
    TriangleMesh () = delete;

    //! load mesh data and build data structure from a file
    TriangleMesh(const char* filename)
    {
        string fn(filename);
        string fext = fn.substr(fn.find_last_of(".") + 1);
        if ( fext == "off") {
            smooth::mesh::loadOFF(filename, vertices, faces, twin, edge_map);
            nV = vertices.size();
            nF = faces.size();
            computeNormals();
            computeValence();
        } else if ( fext == "obj") {
            smooth::mesh::loadOBJ(filename, vertices, tex_coords, normals, faces, twin, edge_map);
            nV = vertices.size();
            nF = faces.size();
            computeValence();
        } else {
            cerr << "Only OBJ and OFF format are supported " << endl;
        }

    }

private:
    //! Compute valence
    void computeValence()
    {
        map<int,int> nelems;
        for (int i=0; i < nF; i++)
        {
            for (unsigned int j=0; j<3; j++)
            {
                ++nelems[faces[i][j]];
            }
        }
        maxValence = 0;
        valence.resize(nV);
        for (auto it = nelems.begin(); it != nelems.end(); ++it)
        {
            valence[it->first] = it->second;
            if (it->second != 4 && !isBoundaryVertex(it->first))
                eops.push_back(it->first);
            if (it->second > maxValence)
                maxValence = it->second;
        }
    }

public:
    /** \brief Find 1-ring around the vertex
     * @param idx index of the vertex
     * @param ov resulting vector 1-ring
     */
    void oneringv(int idx, vector<int> &ov)
    {
        int first_edge = edge(idx);
        int next_edge = first_edge;
        while (1)
        {
            ov.push_back(initial(next_edge));
            next_edge = next(next_edge);
            if (isBoundaryEdge(next_edge)) {
                ov.push_back(terminal(next_edge));
                break;
            }
            next_edge = opposite(next_edge);
            if (next_edge == first_edge) break;
        }

    }
    /** Find 1-ring sorted indices around a vertex vi
    *   Example: vi = 0, the neighbor vertices are 3,4,1 clock-wise, we have array 0,3,4,1.
    *	After sorted we have 0,1,3,4. We want to map the original index to the sorted index.
    *	For instance, the original index of 3 is 1, the sorted index of 3 is 2, so we map 1 -> 2.
    *	Eventually, the output is (the mapping is from index -> element.second)
    *	0 -> (0, 0)
    *	1 -> (1, 2)
    *	2 -> (3, 3)
    *	3 -> (4, 1)
    */
    void oneringIdx(int vi, vector<pair<int,int> >& idx)
    {
        vector<pair<int,int> > orv;
        int inc = 0;
        orv.push_back(pair<int,int>(vi,inc));
        int first_edge = edge(vi);
        int next_edge = first_edge;
        while (1)
        {
            orv.push_back(pair<int,int>(initial(next_edge),++inc));
            next_edge = next(next_edge);
            if (isBoundaryEdge(next_edge)) {
                orv.push_back(pair<int,int>(terminal(next_edge),++inc));
                break;
            }
            next_edge = opposite(next_edge);
            if (next_edge == first_edge) break;
        }

        sort(orv.begin(), orv.end(), [&orv](pair<int,int> l, pair<int,int> r){return l.first < r.first;});
        idx.resize(orv.size());
        for(int i = 0; i < orv.size(); i++)
            idx[orv[i].second] = pair<int,int>(orv[orv[i].second].first,i);
    }
    //! Find 1-ring face of the vertex
    void oneringf(int vi, vector<int> &orf)
    {
        int first_edge = edge(vi);
        int next_edge = first_edge;
        while (1)
        {
            orf.push_back(face(next_edge));
            next_edge = next(next_edge);
            if (isBoundaryEdge(next_edge)) break;
            next_edge = opposite(next_edge);
            if (next_edge == first_edge) break;
        }
    }

    //! incident face of the edge
    inline int face(int edge) { return edge/3;}
    //! initial vertex of the edge
    inline int initial(int edge) { return faces[face(edge)][edge%3];}
    //! terminal vertex of the edge
    inline int terminal(int edge) { return faces[face(edge)][(edge + 1)%3];}
    //! opposite vertex in the triangle
    inline int op_vertex(int edge) { return faces[face(edge)][(edge + 2)%3];}
    //! local index of the edge in the triangle
    inline int local(int edge) { return edge%3;}
    //! next edge in the triangle
    inline int next(int edge) { return 3*(edge/3) + (edge + 1)%3;}
    //! previous edge in the triangle
    inline int prev(int edge) { return 3*(edge/3) + (edge + 2)%3;}
    //! opposite edge (twin edge)
    inline int opposite(int edge) {return twin[edge];}
    //! get edge index from vertex index
    inline int edge(int vertex) { return edge_map[vertex];}

    //! test the edge if it is on boundary
    inline bool isBoundaryEdge(int edge) {return twin[edge] < 0;}
    //! test the edge if it is an inner edge
    inline bool isInnerEdge(int edge) {return twin[edge] >= 0;}
    //! test the vertex if it is on boundary
    inline bool isBoundaryVertex(int vertex) {return isBoundaryEdge(edge(vertex));}
    //!test the face if it is a boundary face
    inline bool isBoundaryFace(int face) {
        for(int i = 0; i < 3; i++)
            if (isBoundaryEdge(3*face+i)) return true;
        return false;
    }
    //!test the if it contains an extra-ordinary point
    inline bool isExtraOrdinaryFace(int face)
    {
        if (isBoundaryFace(face)) return false;
        for(int i = 0; i < 3; i++)
            if (valence[faces[face][i]] != 4) return true;
        return false;
    }
    //! get valence or degree of a vertex
    inline int getValence(int vi) {	return valence[vi];	}

};

#endif
