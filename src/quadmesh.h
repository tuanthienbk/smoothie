#ifndef QUADMESH_H
#define QUADMESH_H

#include "utility.h"
#include <Eigen/SparseCore>

class QuadMesh
{
public:
    int nF,nV, nE;
    VecCoord3 vertices; // vertex coordinates
    VecCoord2 tex_coords; // texture coordinates
    VecCoord3 normals; // normal coordinates
    VecCoord4i faces; // face indices

private:
    vector<int> twin;
    vector<int> edge_map;
    vector<int> valence;
    vector<int> eops;
    int maxValence;
public:

    QuadMesh() = delete;

    QuadMesh(VecCoord3 v, VecCoord4i f): vertices(v), faces(f)
    {
        nF = f.size();
        nV = v.size();
        vector<Eigen::Triplet<int>> tripletList;
        tripletList.reserve(4*nF);
        for(int idx = 0; idx < nF; idx++) {
            Eigen::Vector4i& vertexIndex = f[idx];
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[0],vertexIndex[1],4*idx+1));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[1],vertexIndex[2],4*idx+2));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[2],vertexIndex[3],4*idx+3));
            tripletList.push_back(Eigen::Triplet<int>(vertexIndex[3],vertexIndex[0],4*idx+4));
        }
        Eigen::SparseMatrix<int> edges(nV,nV);
        edges.setFromTriplets(tripletList.begin(), tripletList.end());
        smooth::mesh::buildHalfEdge(edges, nV, nF, twin, edge_map, 4);
        computeValence();
    }
    QuadMesh(const char* filename)
    {
        string fn(filename);
        string fext = fn.substr(fn.find_last_of(".") + 1);
        if ( fext == "off") {
            smooth::mesh::loadOFFQuad(filename, vertices, faces, twin, edge_map);
            nV = vertices.size();
            nF = faces.size();
//            computeNormals();
            computeValence();
        } else if ( fext == "obj") {
            smooth::mesh::loadOBJQuad(filename, vertices, tex_coords, normals, faces, twin, edge_map);
            nV = vertices.size();
            nF = faces.size();
            computeValence();
        } else {
            cerr << "Only OBJ and OFF format are supported " << endl;
        }

    }
    //! incident face of the edge
    inline int face(int edge) { return edge/4;}
    //! initial vertex of the edge
    inline int initial(int edge) { return faces[face(edge)][edge%4];}
    //! terminal vertex of the edge
    inline int terminal(int edge) { return faces[face(edge)][(edge + 1)%4];}
    //! local index of the edge in the quad
    inline int local(int edge) { return edge%4;}
    //! next edge in the quad
    inline int next(int edge) { return 4*(edge/4) + (edge + 1)%4;}
    //! previous edge in the triangle
    inline int prev(int edge) { return 4*(edge/4) + (edge + 3)%4;}
    //! opposite edge (twin edge)
    inline int opposite(int edge) {return twin[edge];}
    //! get edge index from vertex index
    inline int edge(int vertex) { return edge_map[vertex];}
    //! get edge index from vertex index
    int edge(int face, int vertex) {
        for(int i = 0; i < 4; i++)
            if (faces[face][i] == vertex)
                return 4*face + i;
        return -1;
    }
    //! test the edge if it is on boundary
    inline bool isBoundaryEdge(int edge) {return twin[edge] < 0;}
    //! test the vertex if it is on boundary
    inline bool isBoundaryVertex(int vertex) {return isBoundaryEdge(edge(vertex));}
    //!test the face if it is a boundary face
    inline bool isBoundaryFace(int face) {
        for(int i = 0; i < 4; i++)
            if (isBoundaryEdge(4*face+i)) return true;
        return false;
    }
    //!test the if it contains an extra-ordinary point
    inline bool isExtraOrdinaryFace(int face)
    {
        if (isBoundaryFace(face)) return false;
        for(int i = 0; i < 4; i++)
            if (valence[faces[face][i]] != 4) return true;
        return false;
    }
    //! test the edge if it is an inner edge
    inline bool isInnerEdge(int edge) {return twin[edge] >= 0;}
    //! get valence or degree of a vertex
    inline int getValence(int vi) {	return valence[vi];	}

    //! Find vertex list of a face ordered by subdivision rule
    void getSubDivMesh(int fi, vector<int> &orv)
    {
        int i,n,v;

        for(i = 0; i < 4; i++) {
            v = faces[fi][i];
            n = valence[v];
            if ( n != 4) break ;
        }
        if (n ==4 ) v = faces[fi][0];

        orv.push_back(v);
        int e = edge(fi,v);
        for(int k = 0; k < n; k++)
        {
            orv.push_back(terminal(e));
            orv.push_back(terminal(next(e)));
            e = opposite(prev(e));
        }
        e = next(opposite(prev(opposite(e))));
        orv.push_back(terminal(e));
        e = opposite(next(next(e)));
        orv.push_back(terminal(e));
        e = opposite(next(next(e)));
        orv.push_back(terminal(e));
        e = next(e);
        orv.push_back(terminal(e));
        e = next(e);
        orv.push_back(terminal(e));
        e = next(opposite(next(e)));
        orv.push_back(terminal(e));
        e = next(opposite(next(e)));
        orv.push_back(terminal(e));
        return;
    }
    //! Generate refine ring aroud extra-ordinary point
    QuadMesh* generateSubDivRing(int vt)
    {
        int n = valence[vt];
        VecCoord3 rv(12*n+1);
        VecCoord4i rf(9*n);
        vector<int> tr = tworingv(vt);
        //		for(int i = 0 ; i < tr.size(); i++)
        //			cout << tr[i] << " ";
        //		cout << endl;
        rv[0] = (4*n*n - 7*n)*vertices[vt];
        for(int k = 0; k < n; k++)
        {
            rv[12*k+1] = (6*vertices[vt] + 6*vertices[tr[6*k+1]] + vertices[tr[6*k+2]]
                    + vertices[tr[6*((k+1)%n)+1]] + vertices[tr[6*((k-1+n)%n)+1]] + vertices[tr[6*((k-1+n)%n)+2]])/16;
            rv[12*k+2] = (6*vertices[vt] + 36*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]]
                    + vertices[tr[6*((k-1+n)%n)+1]] + 6*vertices[tr[6*((k-1+n)%n)+2]] + vertices[tr[6*((k-1+n)%n)+6]]
                    + 6*vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/64;
            rv[12*k+3] = (vertices[tr[6*((k-1+n)%n)+2]] + vertices[tr[6*((k-1+n)%n)+6]] + 6*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+3]]
                    + vertices[tr[6*k+2]] + vertices[tr[6*k+4]])/16;
            rv[12*k+4] = (vertices[vt] + vertices[tr[6*k+1]] + vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]])/4;
            rv[12*k+5] = (vertices[vt] + 6*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]]
                    + vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/16;
            rv[12*k+6] = (vertices[tr[6*k+1]] + vertices[tr[6*k+2]] + vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/4;
            rv[12*k+7] = (vertices[vt] + vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + 6*vertices[tr[6*((k+1)%n)+1]]
                    + vertices[tr[6*k+6]] + vertices[tr[6*((k+1)%n)+3]])/16;
            rv[12*k+8] = (vertices[vt] + 6*vertices[tr[6*k+1]] + 36*vertices[tr[6*k+2]] + 6*vertices[tr[6*((k+1)%n)+1]]
                    + 6*vertices[tr[6*k+6]] + vertices[tr[6*((k+1)%n)+3]] + vertices[tr[6*k+3]]
                    + 6*vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/64;
            rv[12*k+9] = (vertices[tr[6*k+1]] + vertices[tr[6*k+3]] + 6*vertices[tr[6*k+2]]
                    + 6*vertices[tr[6*k+4]] + vertices[tr[6*k+5]] + vertices[tr[6*k+6]])/16;
            rv[12*k+10] = (vertices[tr[6*((k+1)%n)+1]] + vertices[tr[6*k+6]] + vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+3]])/4;
            rv[12*k+11] = (vertices[tr[6*((k+1)%n)+1]] + 6*vertices[tr[6*k+6]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+3]]
                    + vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/16;
            rv[12*k+12] = (vertices[tr[6*k+2]] + vertices[tr[6*k+6]]+ vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/4;
            rv[0] += 6*vertices[tr[6*k+1]] + vertices[tr[6*k+2]];

            rf[9*k] << 0,12*k+1,12*k+4,12*((k+1)%n)+1;
            rf[9*k+1] << 12*k+1,12*k+2,12*k+5,12*k+4;
            rf[9*k+2] << 12*k+2,12*k+3,12*k+6,12*k+5;
            rf[9*k+3] << 12*((k+1)%n)+1,12*k+4,12*k+7,12*((k+1)%n)+2;
            rf[9*k+4] << 12*k+4,12*k+5,12*k+8,12*k+7;
            rf[9*k+5] << 12*k+5,12*k+6,12*k+9,12*k+8;
            rf[9*k+6] << 12*((k+1)%n)+2,12*k+7,12*k+10,12*((k+1)%n)+3;
            rf[9*k+7] << 12*k+7,12*k+8,12*k+11,12*k+10;
            rf[9*k+8] << 12*k+8,12*k+9,12*k+12,12*k+11;
        }
        rv[0] /= 4*n*n;
        return new QuadMesh(rv,rf);
    }
    //! Find 2-ring of a vertex
    vector<int> tworingv(int vt)
    {
        int n = valence[vt];
        vector<int> rs(6*n+1);
        rs[0] = vt;
        int e = edge(vt);
        e = next(e);
        for(int k = 0; k < n; k++)
        {
            rs[6*k+1] = terminal(e);
            int ee = next(e);
            rs[6*k+2] = terminal(ee);
            ee = next(opposite(ee));
            rs[6*k+3] = terminal(ee);
            ee = next(ee);
            rs[6*k+4] = terminal(ee);
            ee = next(opposite(next(ee)));
            rs[6*k+5] = terminal(ee);
            ee = next(ee);
            rs[6*k+6] = terminal(ee);
            e = opposite(prev(e));
        }
        return rs;
    }
    //! Find 1-ring of the vertex with index vi
    void oneringv(int vi, vector<int> &orv)
    {
        int first_edge = edge(vi);
        int next_edge = first_edge;
        while (1)
        {
            orv.push_back(initial(next_edge));
            next_edge = next(next_edge);
            if (isBoundaryEdge(next_edge)) {
                orv.push_back(terminal(next_edge));
                break;
            }
            next_edge = opposite(next_edge);
            if (next_edge == first_edge) break;
        }

    }
    //! Find 1-ring face of the vertex
    void oneringf(int vi, vector<int> &orf)
    {
        int first_edge = edge(vi);
        int next_edge = first_edge;
        while (1)
        {
            orf.push_back(face(next_edge));
            next_edge = next(next_edge);
            if (isBoundaryEdge(next_edge)) break;
            next_edge = opposite(next_edge);
            if (next_edge == first_edge) break;
        }
    }
private:
    void computeValence()
    {
        map<int,int> nelems;
        for (int i=0; i < nF; i++)
        {
            for (unsigned int j=0; j<4; j++)
            {
                ++nelems[faces[i][j]];
            }
        }
        maxValence = 0;
        valence.resize(nV);
        for (auto it = nelems.begin(); it != nelems.end(); ++it)
        {
            valence[it->first] = it->second;
            if (it->second != 4 && !isBoundaryVertex(it->first))
                eops.push_back(it->first);
            if (it->second > maxValence)
                maxValence = it->second;
        }
    }
};

#endif
