#ifndef BSPLINECURVE_H
#define BSPLINECURVE_H

#include "visualobject.h"

class BsplineCurve: public VisualObject
{
public:
    BsplineCurve(float* cpnts,int nPnt, float* knot, int d)
        :VisualObject()
        , ctrl_pnts(cpnts) //shallow copy, OK since data will be copied to GPU
        , nCtrlPnt(nPnt)
        , knotv(knot) //shallow copy, uniform data, should be careful
        , deg(d)
    {
        setShaderFromFile(PROJECT_PATH"/shaders/bspline_curve.vert"
                          ,PROJECT_PATH"/shaders/bspline_curve.frag"
                          ,PROJECT_PATH"/shaders/bspline_curve.tess"
                          ,PROJECT_PATH"/shaders/bspline_curve.eval");
        glGenBuffers(1, &m_vbo);
        glBindBuffer( GL_ARRAY_BUFFER, m_vbo );
        glBufferData( GL_ARRAY_BUFFER, 3*nCtrlPnt*sizeof(GLfloat), ctrl_pnts, GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        KnotID = glGetUniformLocation(program, "knot_vector");
        DegreeID = glGetUniformLocation(program, "degree");
        nCtrlPntID = glGetUniformLocation(program, "nCtrlPnt");
    }
    virtual ~BsplineCurve(){
        glDeleteBuffers(1, &m_vbo);
    }
    virtual void endRender()
    {
        glBindVertexArray(vao);
        glUniform1fv(KnotID, nCtrlPnt + deg + 1, knotv);
        glUniform1i(DegreeID, deg);
        glUniform1i(nCtrlPntID, nCtrlPnt);
        glBindBuffer( GL_ARRAY_BUFFER, m_vbo);
        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        glEnableVertexAttribArray( 0 );
        glPatchParameteri( GL_PATCH_VERTICES, nCtrlPnt);
        glDrawArrays( GL_PATCHES, 0, nCtrlPnt);
        glBindVertexArray( 0 );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        glDisableVertexAttribArray( 0 );
    }
private:
    GLfloat* ctrl_pnts;
    GLfloat* knotv;
    int deg;
    GLuint DegreeID;
    int nCtrlPnt;
    GLuint KnotID;
    GLuint nCtrlPntID;
    GLuint m_vbo;
};
#endif // BSPLINECURVE_H
