#ifndef COMMON_H
#define COMMON_H

#include "config.h"

#include <GL/glew.h>
#include <Eigen/Dense>

//#define USE_DOUBLE
#ifdef USE_DOUBLE
typedef double Real;
#else
typedef float Real;
#endif

#include <map>
#include <vector>
#include <set>
#include <unordered_map>
#include <unordered_set>

using namespace std;

namespace smooth {
using Vec2r = Eigen::Matrix<Real, 2, 1>;
using Vec3r = Eigen::Matrix<Real, 3, 1>;
using Vec4r = Eigen::Matrix<Real, 4, 1>;
using Vec2i = Eigen::Vector2i;
using Vec3i = Eigen::Vector3i;
using Vec4i = Eigen::Vector4i;
using VecCoord2 = vector<Vec2r>;
using VecCoord3 = vector<Vec3r>;
using VecCoord4 = vector<Vec4r>;
using VecCoord2i = vector<Vec2i>;
using VecCoord3i = vector<Vec3i>;
using VecCoord4i = vector<Vec4i>;
using Mat2 = Eigen::Matrix<Real, 2, 2>;
using Mat3 = Eigen::Matrix<Real, 3, 3>;
using Mat4 = Eigen::Matrix<Real, 4, 4>;
using AlignedBox2 = Eigen::AlignedBox<Real, 2>;
using AlignedBox3 = Eigen::AlignedBox<Real, 3>;
using AngleAxis = Eigen::AngleAxis<Real>;
using Quat = Eigen::Quaternion<Real>;
}

using namespace smooth;

#endif // COMMON_H
