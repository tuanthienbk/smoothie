// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "plot3d.h"
#include "bsplinecurve.h"

int initWindow()
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( 1024, 768, "Trefoil", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    printf("%s %s %s \n", glGetString(GL_VERSION), glGetString(GL_VENDOR), glGetString(GL_RENDERER));

    // Ensure we can capture the escape key being pressed below

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    glfwSetCursorPos(window, 1024/2, 768/2);

    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(10.0);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);

}

int main()
{

    initWindow();

    static GLfloat ctrl_pnts[] =
    {
        -1.f, 0.f, 0.f,
        -0.5f, 0.433f, -0.5f,
        0.5f, 0.433f, 0.5f,
        1.f, 0.f, 0.f,
        0.f, 0.f, -0.5f,
        -0.5f, 0.433f, 0.5f,
        0.f, .866f, 0.f,
        0.5f, 0.433f, -0.5f,
        0.f, 0.f, 0.5f,
        -1.f, 0.f, 0.f,
        -0.5f, 0.433f, -0.5f,
        0.5f, 0.433f, 0.5f
    };
    static GLfloat knots[] = {-0.3f,-0.2f,-0.1f,0.f,0.1f,0.2f,0.3f,0.4f,0.5f,0.6f,0.7f,0.8f,0.9f,1.f,1.1f,1.2f};
    TriangleMesh *tmesh = new TriangleMesh(PROJECT_PATH"/models/boat_tri.obj");
    tmesh->vertices.clear();
    tmesh->vertices.resize(12);
    tmesh->nV = 12;
    for(int i = 0; i < 12; i++) {
        tmesh->vertices[i][0] = ctrl_pnts[3*i];
        tmesh->vertices[i][1] = ctrl_pnts[3*i+1];
        tmesh->vertices[i][2] = ctrl_pnts[3*i+2];
    }
    BsplineCurve *curve = new BsplineCurve(ctrl_pnts,12,knots,3);
    Plot3D *ctrl_polygon = new Plot3D(tmesh);

    glm::mat4 Projection = glm::perspective(45.0f, 3.0f / 3.0f, 0.1f, 100.0f);
    glm::mat4 View       = glm::lookAt(
                glm::vec3(0,0,5.0), // Camera position in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );
    glm::mat4 Model      = glm::mat4(1.0f);
    glm::mat4 MV        =  View * Model;
    do{
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        MV = glm::rotate(MV, 1.0f, glm::vec3(1.0f,0,0));
        // Render control polygon
        ctrl_polygon->beginRender();
        ctrl_polygon->setMatrix(&MV[0][0],"ModelView");
        ctrl_polygon->setMatrix(&Projection[0][0],"Projection");
        ctrl_polygon->endRender();
        // Render Bspline curve
        curve->beginRender();
        curve->setMatrix(&MV[0][0], "ModelView");
        curve->setMatrix(&Projection[0][0], "Projection");
        curve->endRender();
        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 );

    delete curve;
    delete ctrl_polygon;
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    return 0;
}

