#ifndef PLOT3D_H
#define PLOT3D_H

#include "visualobject.h"
#include "trianglemesh.h"

class Plot3D : public VisualObject
{
public:
    Plot3D(GLfloat* cpnts, int d)
        : VisualObject()
        ,pnts(cpnts) //shallow copy, should be careful !!!
        ,nPnt(d)
    {
        setShaderFromFile(PROJECT_PATH"/shaders/lines.vert",PROJECT_PATH"/shaders/lines.frag");
        glGenBuffers(1, &m_vbo);
        glBindBuffer( GL_ARRAY_BUFFER, m_vbo );
        glBufferData( GL_ARRAY_BUFFER, 3*nPnt*sizeof(GLfloat), pnts, GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
    }

    Plot3D(TriangleMesh *m)
        : VisualObject()
    {
        nPnt = m->nV;
        setShaderFromFile(PROJECT_PATH"/shaders/lines.vert",PROJECT_PATH"/shaders/lines.frag");
        glGenBuffers(1, &m_vbo);
        glBindBuffer( GL_ARRAY_BUFFER, m_vbo );
        glBufferData( GL_ARRAY_BUFFER, 3*m->nV*sizeof(GLfloat), m->vertices.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
    }
    virtual ~Plot3D(){
        glDeleteBuffers(1, &m_vbo);
    }

    virtual void endRender()
    {
        glBindVertexArray(vao);
        glBindBuffer( GL_ARRAY_BUFFER, m_vbo);
        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        glEnableVertexAttribArray( 0 );
        glDrawArrays( GL_LINE_STRIP, 0, nPnt);
        glDrawArrays( GL_POINTS, 0, nPnt);
        glBindVertexArray( 0 );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        glDisableVertexAttribArray( 0 );
    }

private:
    GLuint m_vbo;
    GLfloat* pnts;
    GLuint nPnt;
    TriangleMesh *mesh;
};

#endif // PLOT3D_H
