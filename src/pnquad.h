#ifndef PNQUAD_H
#define PNQUAD_H

#include "visualobject.h"
#include "quadmesh.h"

class PNQuad: public VisualObject
{
public:
    PNQuad(QuadMesh* tm):
        VisualObject(),
        mesh(tm),
        hasTexture(false)
    {
        init();
    }

    PNQuad(QuadMesh* tm, const char* tex_file):
        VisualObject(),
        mesh(tm)
    {
        init();
        setTexture(tex_file);
    }

    PNQuad() = delete;

    void setTexture(const char* tex_file) {
        if (mesh->tex_coords.size() == mesh->nV) {
            texture = smooth::mesh::loadDDS(tex_file);
            glGenBuffers(1, &t_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, t_vbo);
            glBufferData(GL_ARRAY_BUFFER, 2*mesh->nV*sizeof(GLfloat), mesh->tex_coords.data(), GL_STATIC_DRAW);
            hasTexture = true;
        } else {
            cerr << "texture coordinate is not found, generate it first" << endl;
        }
    }

    virtual ~PNQuad()
    {
        glDeleteBuffers(1, &m_vbo);
        glDeleteBuffers(1, &n_vbo);
        glDeleteBuffers(1, &i_vbo);
        glDeleteProgram(program);
        if (hasTexture)
        {
            glDeleteBuffers(1, &t_vbo);
            glDeleteTextures(1, &texture);
        }
    }
    virtual void endRender()
    {
        glBindVertexArray( vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, n_vbo);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_vbo);

        if (hasTexture)
        {
            // Bind our texture in Texture Unit 0
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture);
            GLuint TextureID  = glGetUniformLocation(program, "myTextureSampler");
            glUniform1i(TextureID, 0);

            glEnableVertexAttribArray(2);
            glBindBuffer(GL_ARRAY_BUFFER, t_vbo);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }

        glPatchParameteri(GL_PATCH_VERTICES, 4);
        glDrawElements(GL_PATCHES, 4*mesh->nF, GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);

    }

protected:
    GLuint m_vbo, i_vbo, n_vbo, t_vbo, texture;
    bool hasTexture = false;
    QuadMesh *mesh;

private:
    void init()
    {
        setShaderFromFile(PROJECT_PATH"/shaders/pn_quad.vert",
                          PROJECT_PATH"/shaders/pn_quad.frag",
                          PROJECT_PATH"/shaders/pn_quad.tess",
                          PROJECT_PATH"/shaders/pn_quad.eval");

        glBufferData(GL_ARRAY_BUFFER, mesh->nV*sizeof(Vec3r), mesh->vertices.data(), GL_STATIC_DRAW);

        glGenBuffers(1, &i_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_vbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->nF*sizeof(Vec4i), mesh->faces.data(), GL_STATIC_DRAW);

        glGenBuffers(1, &n_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, n_vbo);
        glBufferData(GL_ARRAY_BUFFER, mesh->nV*sizeof(Vec3r), mesh->normals.data(), GL_STATIC_DRAW);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }


};

#endif // PNQUAD_H
